import { ScheduledEvent, Context } from 'aws-lambda'
import { get, post } from '../../../utils/http'

type ShotcallSession = { id: number, title: string, description: string, type: number, game: { id: number, name: string, imagePrefix: string }, name: string, discordWebhook: string }

export const getController = async (_event: ScheduledEvent, _context: Context) => {
    const time = Date.now() - (1000 * 60 * 5)
    const response = await get(`/sessions/discordHooks/after=${time}`)

    if (!response || !response.body || !Array.isArray(response.body)) {
        console.error('Invalid Response @ GetSessions', response)
        throw new Error('Invalid Response @ GetSessions')
    }

    if (response.body.length === 0) {
        console.log('No session created in the last 5 minutes.')
        return { success: true }
    }

    const games = (await get('https://shotcall-lists.s3.amazonaws.com/games/games.json')).body

    if (!games || !Array.isArray(games)) {
        console.error('Invalid Response @ GetGames', games)
        throw new Error('Invalid Response @ GetGames')
    }

    const getHostIds = (data: any[]) => [...new Set(data.map(session => session.hostId))]
    const getHostInfo = (id: number, data: any[]) => {
        const created = data.filter(({ hostId }) => hostId === id).map((session) => ({
            id: session.sessionId,
            title: session.sessionTitle,
            description: session.sessionDescription,
            type: session.sessionType,
            game: games.find(game => game.id === session.gameId),
            name: session.name,
            discordWebhook: session.discordWebhook
        })) as ShotcallSession[]

        return {
            id,
            created,
            name: created[0].name,
            discordWebhook: created[0].discordWebhook
        }
    }

    const hosts = getHostIds(response.body).map(id => getHostInfo(id, response.body as any[]))

    const getMessage = (name: string, created: number) => `${name} has created ${created > 1 ? `${created} new sessions` : 'a new session'}:`

    const discordHooks = []

    for (const host of hosts) {
        const url = host.discordWebhook

        const data = {
            content: getMessage(host.name, host.created.length),
            embeds: host.created.map(session => ({
                color: 1942002,
                title: session.title,
                description: session.description,
                url: `${process.env.SHOTCALL_URL}/session/${session.id}`,
                thumbnail: {
                    url: `https://shotcall-lists.s3.amazonaws.com/games/pics/${session.game.imagePrefix}`
                }
            }))
        }

        const postUpdate = new Promise(resolve => {
            post(url, data).then(resolve).catch(error => {
                console.error('Could not post discord message:', error)
                resolve(null)
            })
        })

        discordHooks.push(postUpdate)
    }

    await Promise.all(discordHooks)

    return {
        success: true
    }
}
