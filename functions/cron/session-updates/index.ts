import { ScheduledHandler } from 'aws-lambda'

import { getController } from './controller'

export const handle: ScheduledHandler = async (event, context) => {
    try {
        await getController(event, context)
    } catch (error) {
        console.error('Event Error:', error)
    }
}
