import { APIGatewayProxyEventV2, Context } from 'aws-lambda'
import { validateDiscordInteraction } from '../../../utils'
import { AuthorizationError } from '../../../utils/errors'

export const getController = async (event: APIGatewayProxyEventV2, _context: Context) => {
    const valid = validateDiscordInteraction({
        body: event.body,
        timestamp: event.headers['x-signature-timestamp'],
        signature: event.headers['x-signature-ed25519']
    })

    if (!valid) throw new AuthorizationError()

    const body = JSON.parse(event.body as string)

    if (body.type === 1) {
        return {
            type: 1
        }
    } else {
        return {
            type: 2,
            data: {
                content: 'Test Message:',
                embeds: [
                    {
                        title: 'This is a test',
                        description: 'It has passed',
                        color: 1942002
                    }
                ]
            }
        }
    }
}
