import { APIGatewayProxyHandlerV2 } from 'aws-lambda'

import Response from '../../../utils/response'
import { getController } from './controller'

export const handle: APIGatewayProxyHandlerV2 = async (event, context) => {
    try {
        const result = await getController(event, context)
        return Response.success(result, context)
    } catch (error) {
        return Response.error(error, context)
    }
}
