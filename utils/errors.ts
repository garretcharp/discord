export class ApplicationError extends Error {
    public statusText: string
    public statusCode: number

    constructor(message = 'An unknown error has occurred. Try again later.', statusCode = 500) {
        super(message)
        this.name = this.constructor.name
        this.statusCode = statusCode
        this.statusText = this.getStatusText()
    }

    private getStatusText() {
        switch(this.statusCode) {
            case 400:
                return 'Bad Request'
            case 401:
                return 'Unauthorized'
            case 403:
                return 'Forbidden'
            default:
                return 'Internal Server Error'
        }
    }
}

export class AuthorizationError extends ApplicationError {
    constructor() {
        super(`This action requires authorization.`, 401)
    }
}

export class BadRequestError extends ApplicationError {
    constructor(message: string) {
        super(message, 400)
    }
}
