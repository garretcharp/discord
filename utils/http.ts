import got from 'got'

export const get = (url: string, headers?: any) => {
    return got.get(`${url.startsWith('http') ? '' : process.env.SHOTCALL_API}${url}`, {
        headers: {
            'Content-Type': 'application/json',
            ...headers
        },
        responseType: 'json'
    })
}

export const post = (url: string, data: Object, headers?: any) => {
    return got.post(`${url.startsWith('http') ? '' : process.env.SHOTCALL_API}${url}`, {
        json: data,
        headers: {
            'Content-Type': 'application/json',
            ...headers
        },
        responseType: 'json'
    })
}
