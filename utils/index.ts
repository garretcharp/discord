import * as nacl from 'tweetnacl'

export const validateDiscordInteraction = ({ body, timestamp, signature }: { body?: string, timestamp?: string, signature?: string }) => {
    if (!body || !timestamp || !signature) return false

    return nacl.sign.detached.verify(
        Buffer.from(timestamp + body),
        Buffer.from(signature, 'hex'),
        Buffer.from(process.env.DISCORD_PUBLIC_KEY as string, 'hex')
    )
}
