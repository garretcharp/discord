import { Context } from 'aws-lambda'
import { ApplicationError } from './errors'

const success = (body: { [key: string]: any }, context: Context) => {
    return {
        body: JSON.stringify(body),
        headers: { 'X-AWS-ID': context.awsRequestId, 'Content-Type': 'application/json' },
        statusCode: 200
    }
}

const error = (e: Error, context: Context) => {
    const appError = e instanceof ApplicationError ? e : new ApplicationError()

    const body = {
        context: {
            requestId: context.awsRequestId,
            service: context.functionName,
            version: context.functionVersion
        },
        error: appError.statusText,
        message: appError.message,
    }

    return {
        body: JSON.stringify(body),
        headers: { 'X-AWS-ID': context.awsRequestId, 'Content-Type': 'application/json' },
        statusCode: appError.statusCode
    }
}

export default { success, error }
